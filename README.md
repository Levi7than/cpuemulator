# CPUemulator
> CPUemulator is application which helps to understand how computers work. It emulates CPU architecture which is based on well known x86.

### Table of contents
* [General info](#general-info)
* [Tech](#tech)
* [Setup](#setup)
* [Development](#development)
* [License](#license)
* [Author](#author)

## General info

Have you ever wondered how is it possible that your computer executes program you told him to? You've surely heard, that computers read numbers, but how is it possible to translate a program you wrote in your favourite programming language into sequence of ones and zeroes? If you want to find out, follow the [wiki pages](https://gitlab.com/encaps.t/cpuemulator/-/wikis/home)!

CPUemulator has been developed in order to demonstrate what are the purposes of all computer parts, such as bus, RAM or CPU. Also how does connection between them works; why and how does CPU read numbers to do operations. I thought the best way to understand is to develop and implement my very own CPU.

## Tech

CPUemulator uses only those technologies to work properly:
* C++20 compiler. Application is guaranteed to work with:
    * MSVC version 16.0
    * GCC version 9.3.0
* CMake version 3.17

##### Tech note
You don't need to know C++ to understand wiki pages.

## Setup

Follow these steps to compile CPUemulator after cloning repository:

```sh
$ cd cpuemulator
$ mkdir build
$ cd build
$ cmake ../
$ make CPUemulator
```

The executable is placed in folder core. In order to run application type:

```sh
$ ./core/CPUemulator <file_to_run>
```

Assembler and disassembler are available as well:
```sh
$ ./assembler/assembler <file_to_compile>
$ ./disassembler/disassembler <file_to_disassemble>
```


## Development

Want to contribute? Great!

Have you seen any bugs or potential improvement? Feel free to send pull request.

### Status
Application in progress. Following features are available:
 - Most of instructions (missing LEA) 
 - Assembler
 - Disassembler
 - Debugger

<!--- ### Todos -->

## License
##### BSD
A small convenience is, that CPUemulator is based on BSD license. You can use fragments of this program wherever you want to (even in personal - not open source projects) the only rule is to mention me as author beforehand.

**Free Software, Hell Yeah!**

## Author
Created and developed by *Artur Twardzik*.