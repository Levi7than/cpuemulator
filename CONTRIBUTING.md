# Contributing to this repository

---

## Code style
 - Identifiers shall be named with ***snake_case***,
 - Class names can be abbreviations,
 - In all child classes use **this** pointer to access parent methods.

Have you any doubts, check Stroustrup convention. Beyond aforementioned rules, any code shall be made using this style.

## Commit messages
All commit messages shall be made using Conventional Commits:
https://www.conventionalcommits.org/en/v1.0.0/
