;;;;;;;;;;; GLOBAL MAIN ;;;;;;;;;;;;;
00                  ; NOP
05 06               ; INC R6
11 00 00 00 19      ; CALL FUNCTION (ADDRESS 25)
06 06               ; DEC R6
00                  ; NOP

31 01 00 00 00 00   ; MOV R1, 0h
31 00 00 00 00 01   ; MOV R0, 1h -> sys_exit
10 80               ; INT 80h

;;;;;;;;;;; FUNCTION ;;;;;;;;;;
05 0E               ; INC R14
05 0F               ; INC R15

31 02 00 00 00 0A   ; MOV R2, 10
22 06 02            ; CMP R6, R2
13 00 00 00 32      ; JE LABEL (ADDRESS 47)
05 06               ; INC R6
12 00 00 00 23      ; JMP LABEL (ADDRESS 35)
02                  ; RET