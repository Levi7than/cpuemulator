;;;;;;;;;;;;; PRINT STRING ;;;;;;;;;;;;;;;;;;;;;;;;;;
55 00 00 AB CD 69 73 00 0A  ; MOV [0000ABCD], "is \n"
55 00 00 AB D1 67 6f 6f 64  ; MOV [0000ABD1], "good"
31 00 00 00 00 04           ; MOV R0, 04 - SYS_WRITE
31 01 00 00 00 01           ; MOV R1, 01 - STDOUT
31 02 00 00 AB CD           ; MOV R2, [0000ABCD] - string begin
31 03 00 00 00 08           ; MOV R3, 8 - number of bytes to write
10 80                       ; INT 80h

;;;;;;;;;; EXIT ;;;;;;;;;;;;;;;
31 00 00 00 00 01   ; MOV R0, 1
27 01 01            ; XOR R1, R1
10 80               ; INT 80h