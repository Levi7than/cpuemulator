//
// Created by Artur Twardzik on 2/27/2021.
//

#ifndef DEBUGGER_DEBUGGER_H
#define DEBUGGER_DEBUGGER_H

#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include <climits>

#include "../../lib/debug.h"
#include "../../core/src/bus.h"
#include "../../disassembler/src/disassembler.h"
#include "../../assembler/src/assembler.h"

class Debugger : bus::bus {
private:
    std::string filename{};
    std::string compiled_file{};
    std::unique_ptr<Disassembler> di;
    uint32_t current_executed_line{1};

    std::vector<std::string> original_file;

private:
    void add_instruction(const uint32_t &line_number, const std::string &instruction);

    void restore_file();

public:
    explicit Debugger(std::string filename, const uint32_t &breakpoint_line = 0,
                      const uint32_t &ram_size = 0xffff);

    void execute_operation(const uint32_t &operation);

    void print_ram();

    std::pair<uint32_t, uint32_t> get_function_location(std::string function_name);

public:
    static inline bool break_debugging{false};

    void print_characters_representation(const std::vector<uint8_t> &text_under_machine_code) const;
};

#endif //DEBUGGER_DEBUGGER_H
