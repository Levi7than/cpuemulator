//
// Created by Artur Twardzik on 12/29/2020.
//

#ifndef CPUEMULATOR_CPU_H
#define CPUEMULATOR_CPU_H

#include <cstdint>
#include <cmath>
#include <array>
#include <vector>
#include <initializer_list>
#include <sstream>
#include <functional>

#include "../../lib/debug.h"
#include "ram.h"

namespace cpu {
    struct memory_unit {
        std::array<int32_t, 16> general_purpose{};
        int32_t eip{0x00000000};
        int32_t esp{};
        int32_t ebp{};
        int32_t esi{};
        int32_t edi{};
        bool cf{};
        bool of{};
        bool sf{};
        bool tf{};
        bool zf{};
    };

    class ALU : public memory_unit {
    public:
        int32_t compute_arithmetic(const uint8_t &opcode,
                                   const int32_t &left_operand,
                                   const int32_t &right_operand = 0);
    };

    class control_unit : ALU {
    private:
        uint8_t instruction{};
        uint16_t instruction_parameters_size{};
        std::vector<uint32_t> parameters{};

        //! returns reference to specified register
        int32_t &reg(const uint32_t &reg);

        //! \description Method converts 4 bytes of given data to single integer.
        //! \param bytes_to_convert Vector of bytes.
        //! \param first Iterator to first byte from range.
        //! \param last Iterator to last byte from range.
        //! \return Single integer composed from 4 bytes.
        int32_t convert_to_single_value(std::vector<uint32_t> &bytes_to_convert,
                                        std::vector<uint32_t>::iterator first,
                                        std::vector<uint32_t>::iterator last) const;

        //! returns reference to special purpose registers
        int32_t &special_purpose_register(const uint32_t &reg);

        //! calls specified system interruption
        void system_interrupt(const uint8_t &number, ram::RAM &RAM);

        //! returns instruction parameter bytes converted into single integer
        //! if no parameters provided, function converts whole vector of instruction parameters
        uint32_t handle_number(uint8_t pos_start = 0, uint8_t pos_end = 0);

        //! Method handles the jump using the logic from given parameter
        //! \param fPtr Pointer to the function with logic
        void handle_jump(const std::function<bool()> &fPtr);

        //! Method handles returning to parrent function
        //! \param amount_to_pop (Optional) parameter, amount of DWORDs to pop from stack after execution
        void handle_ret(ram::RAM &RAM, const uint32_t &amount_to_pop = 0);

        //! Method handles compare operations, affect CPU flags using the result of subtracting parameters
        void handle_cmp(const int32_t &left_operand, const int32_t &right_operand);

        //! Method handles TEST instruction, affect CPU flags using the result of AND gate on given operands
        void handle_test(const int32_t &left_operand, const int32_t &right_operand);

        //! Method calls ALU in order to compute arithmetic operations
        //! \param opcode Opcode of ALU instruction (WARN: the ALU's opcodes are different than CPU)
        void handle_arithmetic(const uint8_t &opcode, bool number = false);

        //! Method handles operations on random access memory
        //! \param where Location where to insert
        //! \param what Value to insert
        //! \param number_of_bytes Number of bytes to assign
        void insert_or_assign(const uint32_t &where,
                              const uint32_t &what,
                              const uint8_t &number_of_bytes,
                              ram::RAM &RAM);

    public:
        void set_esp(uint32_t ram_size);

        bool tf_set() const {
            if (tf) return true;
            return false;
        }

        uint32_t get_eip() const;

        void set_instruction(const uint8_t &opcode);

        uint8_t &get_instruction() { return instruction; }

        //! \description Output of this method represents
        //! parameters of instruction. Depending on instruction, length of parameter may be different
        //! i.e. between 0 and 4 bytes.
        //! \return Number of bytes to read
        uint16_t get_parameters_size() const;

        //! Method sets given parameters as instance variable
        //! \param params Vector of bytes representing the instruction
        void set_instruction_parameters(std::vector<uint32_t> params);

        std::vector<uint32_t> &get_instruction_parameters() { return parameters; }

        void execute(ram::RAM &RAM);

        void print_registers() const;

    public:

        static inline bool sys_exit = false;
    };
}


#endif //CPUEMULATOR_CPU_H
