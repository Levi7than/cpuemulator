//
// Created by Artur Twardzik on 1/2/2021.
//
#include "ram.h"

static std::string dec2hex(const uint32_t &value) {
    std::stringstream str;

    if (value < 16) str << 0;
    str << std::hex << value;

    return str.str();
}

int32_t ram::RAM::get_value(const uint32_t &address, const uint8_t &number_of_bytes) const {
    if (!address_already_exist(address)) {
        throw std::out_of_range(
                debug::format_exception_message(
                        "Segmentation fault. Access violation reading location: %. Address is unassigned.",
                        address
                )
        );
    }

    std::string bytes{}; //while using string we can easily convert it then to integer
    for (size_t i = 0; i < number_of_bytes; ++i) {
        bytes += dec2hex(memory.at(address + i));
    }

    return std::stoll(bytes, nullptr, 16);
}

void ram::RAM::allocate_value(const uint32_t &address, const uint8_t &number_of_bytes, const uint32_t &value) {
    if (address_already_exist(address)) {
        throw std::logic_error(
                debug::format_exception_message(
                        "Access violation writing location: %. Address already assigned.",
                        address
                )
        );
    }
    if (address > memory_size) {
        throw std::logic_error(
                debug::format_exception_message(
                        "Access violation writing location: %. Address is greater then allocated memory.",
                        address
                )
        );
    }

    std::string bytes = dec2hex(value);
    while (bytes.length() < number_of_bytes * 2) bytes.insert(0, "0");

    uint32_t current_address = address;
    for (size_t i = 0; i < number_of_bytes * 2; i += 2) {
        uint8_t byte = std::stoi(bytes.substr(i, 2), nullptr, 16);

        memory.insert(std::make_pair(current_address, byte));
        ++current_address;
    }
}

void ram::RAM::assign_value(const uint32_t &address, const uint8_t &number_of_bytes, const uint32_t &value) {
    if (!address_already_exist(address)) {
        throw std::logic_error(
                debug::format_exception_message(
                        "Segmentation fault. Access violation writing location %. Address is not declared.",
                        address
                )
        );
    }

    std::string bytes = dec2hex(value);
    while (bytes.length() < number_of_bytes * 2) bytes.insert(0, "0");

    uint32_t current_address = address;
    for (size_t i = 0; i < number_of_bytes * 2; i += 2) {
        uint8_t byte = std::stoi(bytes.substr(i, 2), nullptr, 16);

        memory.at(current_address) = byte;
        ++current_address;
    }
}

void ram::RAM::free(const uint32_t &address, const uint8_t &number_of_bytes) noexcept {
    for (size_t i = 0; i < number_of_bytes; ++i) {
        memory.erase(address + i);
    }
}

void ram::RAM::push(const int32_t &value) {
    //stack cells are 4 bytes wide
    stack_head -= 4;
    allocate_value(stack_head, 4, value);
}

int32_t ram::RAM::pop() {
    if (stack_head == memory_size) throw std::out_of_range("Could not pop from empty stack.");

    auto ret = get_value(stack_head, 4);

    free(stack_head, 4);
    stack_head += 4;
    return ret;
}

void ram::RAM::check_stack(const uint32_t &stack_pointer) {
    if (stack_head < stack_pointer) {
        while (stack_head != stack_pointer) pop();
    }
    else if (stack_head > stack_pointer) stack_head = stack_pointer;
}

void ram::RAM::load_program(const std::vector<uint8_t> &instructions) {
    uint32_t address = 0x00;
    for (auto instruction : instructions) {
        memory.insert(std::make_pair(address, instruction));
        ++address;
    }
}
