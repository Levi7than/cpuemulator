//
// Created by Artur Twardzik on 12/29/2020.
//

#ifndef CPUEMULATOR_BUS_H
#define CPUEMULATOR_BUS_H

#include "cpu.h"
#include "ram.h"
#include <fstream>
#include <algorithm>

#include "../../lib/debug.h"

namespace bus {
    class bus {
    protected:
        cpu::control_unit CPU;
        ram::RAM RAM;
    public:
        bus() = default;

        ~bus() = default;

        void load_source_code(const std::string &filename);

        void transfer_instruction();

        void execute_instruction();

        void address_memory(const uint32_t &bytes_to_allocate);

        void print_registers() const { CPU.print_registers(); }

        const std::map<uint32_t, uint8_t> &get_memory() const { return RAM.get_memory(); }

    public:
        static inline bool next_instruction{true};

        static inline bool tf_set{false};

    };

}

#endif //CPUEMULATOR_BUS_H
