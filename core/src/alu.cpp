//
// Created by Artur Twardzik on 2/20/2021.
//
#include "cpu.h"

int32_t cpu::ALU::compute_arithmetic(const uint8_t &opcode, const int32_t &left_operand, const int32_t &right_operand) {
    int64_t temp = 0; //to determine carry and overflow
    int32_t ret = 0;

    auto determine_overflow_or_carry = [&]() {
        if (temp > ret) of = true;
        else of = false;

        if (static_cast<uint32_t>(temp) > static_cast<uint32_t>(ret)) cf = true;
        else cf = false;
    };

    switch (opcode) {
        case 0x00: //MUL
            temp = left_operand * right_operand;
            ret = temp;

            determine_overflow_or_carry();
            break;
        case 0x01: //DIV
            ret = left_operand / right_operand;
            break;
        case 0x02: //ADD
            temp = left_operand + right_operand;
            ret = temp;

            determine_overflow_or_carry();
            break;
        case 0x03: //SUB
            ret = left_operand - right_operand;

            if (static_cast<uint32_t>(right_operand) > static_cast<uint32_t>(left_operand)) cf = true;
            else cf = false;

            if (std::signbit(static_cast<float>(left_operand)) != std::signbit(static_cast<float>(ret))) of = true;
            else of = false;

            break;
        case 0x04: //INC
            ret = left_operand + 1;

            if (ret == 0) {
                of = true;
                cf = true;
            }
            else {
                of = false;
                cf = true;
            }

            break;
        case 0x05: //DEC
            ret = left_operand - 1;
            break;
        case 0x06: //AND
            ret = left_operand & right_operand;
            break;
        case 0x07: //OR
            ret = left_operand | right_operand;
            break;
        case 0x08: //XOR
            ret = left_operand ^ right_operand;
            break;
        case 0x09: //NOT
            ret = ~left_operand;
            break;
        case 0x0A: //SHL
            ret = left_operand << right_operand;
            break;
        case 0x0B: //SHR
            ret = left_operand >> right_operand;
            break;
    }

    if (ret == 0) zf = true;
    else zf = false;

    if (ret < 0) sf = true;
    else sf = false;

    return ret;
}