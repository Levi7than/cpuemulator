//
// Created by Artur Twardzik on 1/25/2021.
//

#ifndef TESTS_ASSEMBLER_H
#define TESTS_ASSEMBLER_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <map>

#include "../../lib/debug.h"

struct Instruction {
    uint32_t instruction_address{};
    uint32_t occur_line{};
    std::string parent_namespace{};
    std::string occur_file{};

    std::string keyword{};
    std::string left_ptr_size{};
    std::string left{};
    std::string right{};
    std::string right_ptr_size{};

    bool operator!=(const Instruction &other) const {
        if (this->keyword == other.keyword &&
            this->left_ptr_size == other.left_ptr_size && this->left == other.left &&
            this->right_ptr_size == other.right_ptr_size && this->right == other.right) {
            return false;
        }
        return true;
    }

    //! Function concatenates tokenized parts of instruction to readable data
    //! \return String containing assembly instruction
    inline std::string get_instruction() const {
        std::string ret = keyword + " " +
                          (!left_ptr_size.empty() ? left_ptr_size + " " : "") +
                          left + (!right.empty() ? "," : "") +
                          (!right_ptr_size.empty() ? right_ptr_size + " " : "") +
                          (!right.empty() ? right : "");

        return ret;
    }
};

struct Variable {
    uint32_t address{};
    std::string name{};
    std::string bytes{};
};

class Label {
    std::string label{};
    std::string occur_file{};
    uint32_t address{};
    uint32_t occur_line{};

public:
    Label() = delete;

    explicit Label(const std::string &label_name, const std::string &file, const uint32_t &line = 0)
            : label(label_name), occur_file(file), occur_line(line) {}

    void set_address(const uint32_t &new_address) { address = new_address; }

    const uint32_t &get_occur_line() const { return occur_line; }

    const std::string &get_occur_file() const { return occur_file; }

    std::string get_address() const {
        std::stringstream addr;
        addr << std::hex << std::uppercase << address;

        std::string ret = addr.str();
        while (ret.length() < 8) ret.insert(0, "0");

        return ret;
    }

    std::string get_label_name() const { return label; }

    inline bool empty() const {
        if (label.empty()) return true;
        return false;
    }

    bool operator==(const std::string &other) const {
        if (other == label) return true;
        return false;
    }

    bool operator==(const Label &other) const {
        if ((other.label == label) && (other.address == address)) return true;
        return false;
    }
};

class Assembler {
private:
    std::vector<Instruction> instructions;

    std::vector<std::string> external_includes;

    std::vector<std::string> machine_codes;

    std::vector<Variable> variables;

    std::vector<Label> labels;

    std::multimap<std::string, Label> sublabels;

    std::string main_file{};

    std::string executable{};

    std::string working_directory{};

    uint32_t address{};

    uint32_t current_executed_line{};

    std::string current_executed_file{};

    [[maybe_unused]] uint32_t total_lines{};

    bool debug_mode;

private:
    enum section {
        TEXT, DATA, BSS
    };

private:
    //! This method loads specified file to the program memory.
    //! \param filename Location of the file to open.
    void load_file(const std::string &filename);

    //! This method deletes both white characters from begin/end and comments.
    //! Except string literals, whole line is converted to uppercase letters.
    //! \param line Current line from file.
    void prepare_line(std::string &line);

    //! This method tokenizes basic instructions (those from section text)
    //! \param line Line to tokenize from file
    //! \param instruction Reference to the object of struct Instruction
    void tokenize(const std::string &line, Instruction &instruction);

    //! This method tokenizes instructions from data section (these are not real assembly instructions)
    //! \param line Line to tokenize from file
    void tokenize_data(std::string line);

    //! This method tokenizes instructions from bss section
    //!  it is separated from data section, because of difference
    //!  in functioning (bss has to allocate specified number of bytes)
    //! \param line Line to tokenize from file
    void tokenize_bss(const std::string &line);

    //! This method tokenizes files to include
    //! \param line String containing filename of file to include
    void tokenize_include(const std::string &line);

    void append_includes();

    //! This method translates given (tokenized) instruction into its machine code version
    //! \param current Instruction to tokenize
    //! \return String containing machine code instruction of a single line
    std::string transform_to_opcodes(Instruction &current);

    //! At the end of translating whole file this function finds usages
    //!  of labels and replace their addresses. <br><br>
    //!  Since the address of a function in assembly is not known at the beginning of compilation,
    //!  the compiler has to put 0x00000000 as temporary value and if it finds the first usage of label,
    //!  those zeroes are replaced with valid address. <br><br>
    //! This method may be not optimal, due to the fact, that it does not matter where the declaration is;
    //!  the labels can be accessed even before declaration.
    //! \param current The Instruction object, which contains first appearance of a label (its address)
    void update_labels_addresses();

    //! This method assigns address to given label depending on the occur line and file.
    //! \param label Reference to the label which will be addressed.
    void assign_address_to_label(Label &label);

    //! Thie method replaces specified bytes of machine code with specified value.
    //! \param instruction Reference to string containing instruction.
    //! \param value Value to paste.
    //! \param first_half_byte First half-byte to replace (number of character in string representation)
    void change_machine_code(std::string &instruction, const std::string &value, uint8_t first_half_byte);

    //! At the end of translating whole file this function assign
    //!  addresses to declared variables and replace the appearance of their names
    //!  with their addresses. The variables are placed at the end of executable.
    void update_variables_addresses();

    std::string prepare_addressing(std::string line);

    //! This method converts given register to its number.
    //! \param reg The string which contains register R(1-15), ESI, EDI, ESP, EBP, EIP
    //! \return Number of a register
    uint8_t transform_register_to_num(const std::string &reg);

    //! This method handles the conversion of assembly instruction which contains
    //   the register inside into machine code.
    //! \param opcode Opcode of the instruction
    //! \param reg_l_number Left register.
    //! \param reg_r_number (OPTIONAL) Right register.
    //! \return Machine code of the instruction.
    std::vector<uint8_t> handle_reg(const uint8_t &opcode,
                                    const std::string &reg_l_number,
                                    const std::string &reg_r_number = "");

    //! This method handles the conversion of assembly instruction which contains
    //!  register as left parameter and some decimal or hexadecimal value as a right parameter
    //!  into machine code.
    //! \param opcode Opcode of the instruction.
    //! \param reg Left register.
    //! \param value Right parameter, which contains dec or hex number.
    //! \return Machine code of the instruction.
    std::vector<uint8_t> handle_reg_value(const uint8_t &opcode,
                                          const std::string &reg,
                                          const std::string &value);

    //! This method handles the conversion of assembly instruction which contains jump to specified label.
    //! \param opcode Opcode of the instruction.
    //! \param instruction Const reference to the current instruction.
    //! \return Machine code of the instruction.
    std::vector<uint8_t> handle_jump(const uint8_t &opcode, const Instruction &instruction);

    //! This method handles the conversion of assembly instruction which contains mathematical
    //! (arithmetic and logic) operations.
    //! \param opcode Opcode if right operand is a register. If not + 0x10 automatically added.
    //! \param instruction Const reference to the current instruction.
    //! \return Machine code of the instruction.
    std::vector<uint8_t> handle_math(const uint8_t &opcode, const Instruction &instruction);

    //! This method handles numbers. It determines whether number is decimal or hexadecimal.
    //! \param number The number to convert.
    //! \return DECIMAL numbers, but stored as single bytes
    std::vector<uint8_t> handle_dec_hex(const std::string &number);

    //! This method handles addressing as the left parameter of the instruction.
    //! \param instruction Instruction to handle.
    //! \return Machine code of the instruction.
    std::vector<uint8_t> handle_left_size_ptr(Instruction instruction);

    //! This method handles addressing as the right parameter of the instruction.
    //! \param instruction Instruction to handle.
    //! \return Machine code of the instruction.
    std::vector<uint8_t> handle_right_size_ptr(const Instruction &instruction);

    //! This method handles variables while translating text section. It checks whether
    //!  given symbol exist, if it does it puts 0x00000000, if not thorws exception.
    //! \param opcode Opcode of the instruction.
    //! \param variable_name Symbol which might be a variable.
    //! \param destination Destination register.
    //! \return Machine code of the instruction.
    std::vector<uint8_t> handle_variable(uint8_t opcode,
                                         const std::string &variable_name,
                                         const std::string &destination = "");

    //! This method handles indirect addressing of a location.
    //! \param indirect_address The concatenate of registers with other registers or numbers.
    //! \return Machine code of the instruction.
    std::vector<uint8_t> convert_from_indirect(const std::string &indirect_address);

    //! Conversion from decimal to hexadecimal
    //! \param number String containing number to convert.
    //! \return String with converted to hex number.
    std::string dec2hex(const std::string &number) const;

    //! If the assembler finds string literal it tries to convert it to the hexadecimal
    //!  version of the characters inside with this method.
    //! \param text String literal to convert.
    //! \return Sequence of bytes representing letters from string.
    std::string string2hex(const std::string &text);

    //! This method generates error message with line number and underlined place of error.
    //! \param message Error message.
    //! \return Whole formatted error message.
    std::string generate_error_message(const std::string &message) const;

    //! This method generates release machine code - without any comments
    void save_release();

    //! This method generates machine code with debug mode comments i.e.<br>
    //!  - assembly comments <br>
    //!  - labels' names <br>
    //!  - variables' names <br>
    void save_debug();

    //! This method indicates whether given line contains two quotation marks
    //! \param line Line from file.
    //! \return True if string literal exist.
    inline bool contains_string_literal(const std::string &line) {
        int num = std::count(line.begin(), line.end(), '"');

        return num;
    }

    //! This method indicates whether given string starts end ends with quotation mark.
    //! \param s String to check.
    //! \return True if string contains string literal.
    inline bool is_string_literal(const std::string &s) {
        if (s.starts_with('"') && s.ends_with('"')) return 1;
        return 0;
    }

    //! This method indicates whether given string contains numbers only (hex and dec)
    //! \param s String to check
    //! \return True if string contains only numbers.
    inline bool is_number(const std::string &s) {
        int num_dec = std::count_if(s.begin(), s.end(), [](unsigned char c) {
            return std::isdigit(c);
        });

        int num_hex = std::count_if(s.begin(), s.end(), [](unsigned char c) {
            return std::isxdigit(c);
        });


        if (num_dec == s.length()) return true;
        else if ((num_hex == s.length() - 1) && s.ends_with('H')) return true;
        else return false;
    }

    //! This method indicates whether given string is convertible to the register.
    //! \param s String which might contain register.
    //! \return True if string contains register.
    inline bool is_register(const std::string &s) {
        try {
            if (is_number(s)) return 0; //checks whether given string is a pure number

            auto ret = transform_register_to_num(s);
            return 1;
        }
        catch (std::logic_error &e) {
            return 0;
        }
    }

    //! This method indicates whether given string contains indirect addressing (either + or -)
    //! \param s String which might contain indirect addressing.
    //! \return True if string contains indirect addressing.
    bool is_indirect_addressing(const std::string &s) {
        if (s.find('+') != std::string::npos) return 1;
        else if (s.find('-') != std::string::npos) return 1;
        else return 0;
    }

    //! Method checks whether variable with given name exist
    //! \param var_name
    //! \return
    inline bool variable_exist(std::string var_name) const {
        if (var_name.starts_with('[')) var_name = var_name.substr(1, var_name.length() - 2);

        int count = std::count_if(variables.begin(), variables.end(), [&](const Variable &v) {
            return (var_name == v.name);
        });

        return count;
    }

public:
    Assembler() = delete;

    Assembler(std::string filename, const std::string &output_file = "", bool comments = false);

    //! This function starts conversion of assembly to machine code and writes
    //! executable to the file with the same name as assembly, but with .ce extension.
    void run();
};

#endif //TESTS_ASSEMBLER_H
